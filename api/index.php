<?php 

$data = [];

// Request 
if(isset($_GET['option'])) {
    
    switch ($_GET['option']) {
        case 'status':
            $data['status'] = 'SUCCESS';
            $data['data']   = 'API is running ...';
        break;

        default: 
            $data['status'] = 'ERROR';
            break;
    }

} else {
    $data['status'] = 'ERROR';
}

// Show the API Response
response($data);


// Response
function response($data_response)
{
    header("Content-Type:application/json");
    echo json_encode($data_response);
}
